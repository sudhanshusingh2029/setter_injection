package com.selenium;


import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Client {

    public static void main(String[] args) {
        ApplicationContext context = new ClassPathXmlApplicationContext("beans.xml") ;
        System.out.println("hi");
        Student st = context.getBean("stu" , Student.class) ;
        st.cheating() ;
    }
}
